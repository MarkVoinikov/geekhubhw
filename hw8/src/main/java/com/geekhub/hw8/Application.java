package com.geekhub.hw8;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Application {

    public static void main(String[] args) throws IOException {
        String sourceDirPath = inputWorkDir();
        String archiveName1 = "audios";
        String archiveName2 = "images";
        String archiveName3 = "videos";
        final Path start = Paths.get(sourceDirPath);

        final List<File> collectAudio = Files.walk(start)
                .filter(path -> path.toString().endsWith(".mp3") || path.toString().endsWith(".wav"))
                .map(Path::toFile)
                .collect(Collectors.toList());
        System.out.println(collectAudio);
        final List<File> collectImage = Files.walk(start)
                .filter(path -> path.toString().endsWith(".jpg") || path.toString().endsWith(".png")
                        || path.toString().endsWith(".gif") || path.toString().endsWith(".jpeg"))
                .map(Path::toFile)
                .collect(Collectors.toList());
        System.out.println(collectImage);
        final List<File> collectVideo = Files.walk(start)
                .filter(path -> path.toString().endsWith(".mp4") || path.toString().endsWith(".flv")
                        || path.toString().endsWith(".avi"))
                .map(Path::toFile)
                .collect(Collectors.toList());
        System.out.println(collectVideo);

        zipFiles(archiveName1, collectAudio, sourceDirPath);
        zipFiles(archiveName2, collectImage, sourceDirPath);
        zipFiles(archiveName3, collectVideo, sourceDirPath);
    }

    private static void zipFiles(String archiveName, List<File> files, String sourceDirPath) {
        try (ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(sourceDirPath
                                                        + "\\" + archiveName + ".zip")))) {
            for (File file : files) {
                zos.putNextEntry(new ZipEntry(file.getAbsolutePath().substring(sourceDirPath.length()+1)));
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = fileInputStream.read(buffer)) > 0) {
                    zos.write(buffer, 0, length);
                }
                fileInputStream.close();
                zos.closeEntry();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String inputWorkDir() {
        System.out.println("Enter full path to files: ");
        Scanner reader = new Scanner(System.in);
        return reader.nextLine();
    }
}

