package com.geekhub.hw2;

interface Shape {

    double calculateArea();

    double calculatePerimeter();

}

