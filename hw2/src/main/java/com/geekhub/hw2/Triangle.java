package com.geekhub.hw2;

class Triangle implements Shape {
    private static double sizeA = 0;
    private static double sizeB = 0;
    private static double sizeC = 0;

    static void setSizeA(double a) {
        sizeA = a;
    }

    static void setSizeB(double b) {
        sizeB = b;
    }

    static void setSizeC(double c) {
        sizeC = c;
    }

    public double calculatePerimeter() {
        return sizeA + sizeB + sizeC;
    }

    public double calculateArea() {
        double p = calculatePerimeter() / 2;
        return Math.sqrt(p * (p - sizeA) * (p - sizeB) * (p - sizeC));
    }
}
