package com.geekhub.hw2;

public enum Shapes {
    CIRCLE("Circle"),
    RECTANGLE("Rectangle"),
    SQUARE("Square"),
    TRIANGLE("Triangle");

    public String parameterName;

    Shapes(String parameterName) {
        this.parameterName = parameterName;
    }

    public static Shapes fromString(String parameterName) throws NullPointerException {
        if (parameterName != null) {
            for (Shapes objType : Shapes.values()) {
                if (parameterName.equalsIgnoreCase(objType.parameterName)) {
                    return objType;
                }
            }
        }
        throw new NullPointerException("Wrong input");
    }
}

