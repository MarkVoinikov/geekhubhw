package com.geekhub.hw2;

class Circle implements Shape {
    private static double radius = 0;

    static void setRadius(double a) {
        radius = a;
    }

    public double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }

   public double calculateArea() {
        return Math.PI * Math.pow(radius, 2);
   }
}
