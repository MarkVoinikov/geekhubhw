package com.geekhub.hw2;

class Rectangle extends Square {
    private static double sizeB = 0;

    static void setSizeB(double b) {
        sizeB = b;
    }

    static double getSizeB() {
        return sizeB;
    }

    public double calculateArea() {
        return sizeB * getSizeA();
    }

    public double calculatePerimeter() {
        return 2 * (sizeB + getSizeA());
    }
}
