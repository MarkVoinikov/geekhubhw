package com.geekhub.hw2;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Shapes arg = Shapes.fromString(inputNameShape());
        switch (arg) {
            case SQUARE:
                Shape shape = new Square();
                System.out.println("Enter size A: ");
                Square.setSizeA(inputValue());
                sayArea(shape.calculateArea());
                sayPerimeter(shape.calculatePerimeter());
                System.out.println("Also this square has diagonal: " + Square.calculateDiagonal
                        (Square.getSizeA(), Square.getSizeA()));
                break;
            case CIRCLE:
                Shape shape1 = new Circle();
                System.out.println("Enter size A: ");
                Circle.setRadius(inputValue());
                sayArea(shape1.calculateArea());
                sayPerimeter(shape1.calculatePerimeter());
                break;
            case TRIANGLE:
                Shape shape2 = new Triangle();
                System.out.println("Enter size A: ");
                Triangle.setSizeA(inputValue());
                System.out.println("Enter size B: ");
                Triangle.setSizeB(inputValue());
                System.out.println("Enter size C: ");
                Triangle.setSizeC(inputValue());
                sayArea(shape2.calculateArea());
                sayPerimeter(shape2.calculatePerimeter());
                break;
            case RECTANGLE:
                Shape shape3 = new Rectangle();
                System.out.println("Enter size A: ");
                Rectangle.setSizeA(inputValue());
                System.out.println("Enter size B: ");
                Rectangle.setSizeB(inputValue());
                sayArea(shape3.calculateArea());
                sayPerimeter(shape3.calculatePerimeter());
                System.out.println("Also this rectangle has diagonal: " + Rectangle.calculateDiagonal
                        (Rectangle.getSizeA(), Rectangle.getSizeB()));
                break;
        }
    }

    private static void sayArea(double a) {
        System.out.println("area is: " + a);
    }

    private static void sayPerimeter(double a) {
        System.out.println("perimeter is: " + a);
    }

    private static String inputNameShape() {
        System.out.println("Please select shape and write down: CIRCLE, RECTANGLE, SQUARE, TRIANGLE");
        Scanner reader = new Scanner(System.in);
        return reader.nextLine();
    }

    private static double inputValue() {
        String VALUE_VALIDITY = "^(\\d+)$";
        Scanner reader = new Scanner(System.in);
        String number = reader.nextLine();
        boolean isValid = number.matches(VALUE_VALIDITY);
        while (!isValid) {
            System.out.println("This is not a number. Please enter the number: ");
            number = reader.nextLine();
            isValid = number.matches(VALUE_VALIDITY);
        }
        return Double.parseDouble(number);
    }
}
