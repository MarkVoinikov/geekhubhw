package com.geekhub.hw2;

class Square implements Shape {
    private static double sizeA =0;

    static void setSizeA(double a) {
        sizeA = a;
    }

    static double getSizeA() {
        return sizeA;
    }

    public double calculateArea() {
        return 2 * sizeA;
    }

    public double calculatePerimeter() {
        return 4 * sizeA;
    }

    static double calculateDiagonal(double a, double b){
        return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    }
}
