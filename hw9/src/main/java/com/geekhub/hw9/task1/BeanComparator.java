package com.geekhub.hw9.task1;

import com.sun.org.apache.xpath.internal.operations.Equals;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BeanComparator {

    public static void fieldsCompare(Object x, Object y) throws IllegalAccessException {
        if (!x.getClass().equals(y.getClass())) {
            System.out.println("It is different classes");
        } else {
            Field[] nameFields1 = x.getClass().getDeclaredFields();
            for (Field field1 : nameFields1) {
                field1.setAccessible(true);
                Object value = field1.get(x);
                Object value1 = field1.get(y);
                if (value.equals(value1)) {
                    System.out.println(field1.getName() + ": " + value + "<--->" + value1 + " - " + true);
                } else {
                    System.out.println(field1.getName() + ": " + value + "<--->" + value1 + " - " + false);
                }
            }
        }
    }
}

