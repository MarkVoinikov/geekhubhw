package com.geekhub.hw9.task1;

import java.lang.reflect.*;

public class CloneCreator {
    public static void createClone(Object args) throws IllegalAccessException {
        Field[] nameFields = args.getClass().getDeclaredFields();
        for (Field field : nameFields) {
            field.setAccessible(true);
            Object value = field.get(args);
            System.out.println(field.getName() + ": " + value);
        }
    }
}

