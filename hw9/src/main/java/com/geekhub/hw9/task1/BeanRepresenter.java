package com.geekhub.hw9.task1;

import java.lang.reflect.*;

class BeanRepresenter {

    public static void createRepresenter(Class object, Object args) {
        Field[] nameFields = object.getDeclaredFields();
        for (Field field : nameFields) {
            field.setAccessible(true);
            Object value = null;
            try {
                value = field.get(args);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            System.out.println(field.getName() + ": " + value);
        }
    }
}
