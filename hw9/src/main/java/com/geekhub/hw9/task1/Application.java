package com.geekhub.hw9.task1;


public class Application {

    public static void main(String[] args) throws IllegalAccessException {
        Cat cat1 = new Cat("brawn", 2, 4, 60);
        Cat cat2 = new Cat("brawn", 4, 4, 72);
        Human human1 = new Human(170, "black", 30, 60);
        Car car1 = new Car("black", 230, "sedan", "RX-7");
        Car car2 = new Car("yellow", 220, "sedan", "RX-6");

        BeanRepresenter.createRepresenter(Cat.class, cat1);
        BeanRepresenter.createRepresenter(Human.class, human1);
        BeanRepresenter.createRepresenter(Car.class, car1);
        CloneCreator.createClone(cat1);
        BeanComparator.fieldsCompare(cat1, cat2);
        BeanComparator.fieldsCompare(car1, car2);
    }
}

