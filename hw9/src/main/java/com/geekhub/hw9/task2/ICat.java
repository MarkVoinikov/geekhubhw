package com.geekhub.hw9.task2;

public interface ICat {
    @TraceArgs
    String getColor();

    @TraceArgs
    @MeasureTime
    int getAge();

    @MeasureTime
    @TraceArgs
    int getLegCount();

    @TraceArgs
    @MeasureTime
    int getFullLength();

    @LimitInvocations(times = 2)
    void print();
}
