package com.geekhub.hw9.task2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.time.LocalTime;
import java.util.*;

public class Handler implements InvocationHandler{

    private Object obj;

    static Object newInstance(Object obj) {
        return java.lang.reflect.Proxy.newProxyInstance(
                obj.getClass().getClassLoader(),
                obj.getClass().getInterfaces(),
                new Handler(obj));
    }

    private Handler(Object obj) {
        this.obj = obj;
    }

    private Map<Method, Integer> instances = new HashMap<>();

    @Override
    public Object invoke(Object proxy, Method method, Object [] args) throws Throwable {
        instances.putIfAbsent(method, 0);
        if (method.isAnnotationPresent(MeasureTime.class)) {
            int start = LocalTime.now().getSecond();
            method.invoke(obj, args);
            int end = LocalTime.now().getSecond();
            System.out.println("Executed in: " + (end - start) + "sec");
        }
        if (method.isAnnotationPresent(TraceArgs.class)) {
            System.out.println("Method name: " + method.getName());
        }
        LimitInvocations limit = method.getAnnotation(LimitInvocations.class);
        if (method.isAnnotationPresent(LimitInvocations.class)) {
            while (limit.times() > instances.get(method)) {
                instances.merge(method, 1, (a, b) -> a + b);
                method.invoke(obj, args);
            }
            return args;
        }
        return method.invoke(obj, args);
    }
}


