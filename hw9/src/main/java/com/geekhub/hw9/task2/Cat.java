package com.geekhub.hw9.task2;

public class Cat implements ICat {
    private final String color;
    private int age;
    private int legCount;
    private int fullLength;

    Cat(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }

    public String getColor() {
        return color;
    }

    public int getAge() {
        return age;
    }

    public int getLegCount() {
        return legCount;
    }

    public int getFullLength() {
        try {
           Thread.sleep(10000);
       } catch (InterruptedException e) {
           e.printStackTrace();
       }
        return fullLength;
    }

    public void print() {
        System.out.println("ty-ty-la ty-ty la-la-la");
    }
}

