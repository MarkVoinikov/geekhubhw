package com.geekhub.hw4.task1;

public class Application {

    public static void main(String[] args) {
        List<User> users = new LinkedList<>();

        int size = users.size();
        if (size != 0) {
            throw new RuntimeException();
        }

        boolean empty = users.isEmpty();
        if (!empty) {
            throw new RuntimeException();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        User user1 = new User("user1");

        users.add(user1);

        size = users.size();
        if (size != 1) {
            throw new RuntimeException();
        }

        empty = users.isEmpty();
        if (empty) {
            throw new RuntimeException();
        }


        ////////////////////////////////////////////////////////////////////////////////////////////

        //MUST PRINT User{name = user1}
        users.print();
        System.out.println();
        ////////////////////////////////////////////////////////////////////////////////////////////

        users.remove(user1);
        System.out.println();


        size = users.size();
        if (size != 0) {
            throw new RuntimeException();
        }

        empty = users.isEmpty();
        if (!empty) {
            throw new RuntimeException();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        User user2 = new User("user2");
        User user3 = new User("user3");

        users.add(user2);
        users.add(user3);

        size = users.size();
        if (size != 2) {
            throw new RuntimeException();
        }

        empty = users.isEmpty();
        if (empty) {
            throw new RuntimeException();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////


        //MUST PRINT User{name = user2}, User{name = user3}
        users.print();
        System.out.println();

        ////////////////////////////////////////////////////////////////////////////////////////////
        User user4 = new User("user4");

        users.remove(0);

        System.out.println();
        users.add(user4);


        //MUST PRINT User{name = user3}, User{name = user4}
        users.print();
        System.out.println();

        size = users.size();
        if (size != 2) {
            throw new RuntimeException();
        }

        empty = users.isEmpty();
        if (empty) {
            throw new RuntimeException();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        User user = users.get(1);
        if (user != user4) {
            throw new RuntimeException();
        }

        //////////////////////////////////////////////////////////////////////////////////////////

        try {
            users.remove(1);
            users.remove(1);
        } catch (IndexOutOfBoundsException e) {
            //OK
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        size = users.size();

        if (size != 1) {
            throw new RuntimeException();
        }

        empty = users.isEmpty();
        if (empty) {
            throw new RuntimeException();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////

        users.remove(0);

        size = users.size();
        if (size != 0) {
            throw new RuntimeException();
        }

        empty = users.isEmpty();
        if (!empty) {
            throw new RuntimeException();
        }
    }
}
