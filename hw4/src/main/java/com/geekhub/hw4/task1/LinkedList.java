 package com.geekhub.hw4.task1;

public class LinkedList<E> implements List<E> {

    private Node<E> head;
    private Node<E> tail;
    private int size = 0;

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);
        if (head == null) {
            head = newNode;
            tail = newNode;
        } else {
            tail.setNext(newNode);
            tail = newNode;
        }
        this.size++;
    }

    @Override
    public boolean remove(E element) {
        Node<E> prev = null;
        Node<E> curr = head;
        while (curr != null) {
            if (curr.getValue().equals(element)) {
                if (prev != null) {
                    prev.setNext(curr.getNext());
                    if (curr.getNext() == null) {
                        tail = prev;
                    }
                } else {
                    head = head.getNext();
                    if (head == null) {
                         tail = null;
                    }
                }
                size--;
                return true;
            }
            prev = curr;
            curr = curr.getNext();
        }
        return false;
    }

    @Override
    public void remove(int index) {
        Node<E> prev = tail;
        Node<E> current = head;
        if (index < 0 || index >= size()) {
            System.out.println("Wrong index");
        } else if (index == 0) {
            head = head.getNext();
            current.setNext(null);
            --size;
        } else {
            for (int i = 0; i < index; i++) {
                prev = current;
                current = current.getNext();
            }
            prev.setNext(current.getNext());
            --size;
        }
    }

    @Override
    public E get(int index) {
        Node<E> current = head;
        int count = 0;
        while (count < index) {
            count++;
            current = current.getNext();
        }
        return current.getValue();
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void print() {
        Node<E> current = head;
        while(current != null) {
            System.out.print(current.getValue() + ", ");
            current = current.getNext();
        }
    }
}
