package com.geekhub.hw4.task2;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<User> data = new ArrayList<>();

        data.add(new User("B", 132));
        data.add(new User("A", 234));
        data.add(new User("C", 357));
        data.add(new User("D", 246));
        data.add(new User("I", 789));
        for (User e : data) {
            System.out.print(e.toString());
        }
        System.out.println();
        System.out.println("Now:");
        System.out.println(ListSorter.sortByBubbles(data, Direction.ASC));
        System.out.println(ListSorter.sortByBubbles(data, Direction.DESC));
        System.out.println(ListSorter.sortByInsertion(data, Direction.ASC));
        System.out.println(ListSorter.sortByInsertion(data, Direction.DESC));
        System.out.println(ListSorter.sortBySelection(data, Direction.ASC));
        System.out.println(ListSorter.sortBySelection(data, Direction.DESC));
    }
}
