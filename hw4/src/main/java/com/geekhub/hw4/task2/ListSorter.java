package com.geekhub.hw4.task2;

import java.util.*;

class ListSorter {

    public static <T extends Comparable> List<T> sortByBubbles(List<T> elements, Direction direction) {
        for (int size = elements.size(); size != 1; --size) {
            for (int i = 0; i < size - 1; i++) {
                T temp1 = elements.get(i + 1);
                T temp2 = elements.get(i);
                if ((direction.getDirection() * temp1.compareTo(temp2)) < 0) {
                    elements.set(i, temp1);
                    elements.set(i + 1, temp2);
                }
            }
        }
        return elements;
    }

    public static <T extends Comparable> List<T> sortByInsertion(List<T> elements, Direction direction) {
        int i, j;
        for (i = 1; i < elements.size(); i++) {
            T temp = elements.get(i);
            j = i - 1;
           // int l = k.compareTo(temp);
           // System.out.println(k.compareTo(temp));
            while ((j >= 0) && (elements.get(j).compareTo(temp) * direction.getDirection() > 0)) {
                elements.set(j + 1, elements.get(j));
                j -= 1;
            }
            elements.set(j + 1, temp);
        }
        return elements;
    }

   public static <T extends Comparable> List<T> sortBySelection(List<T> elements, Direction direction) {
       for(int k = 0; k < elements.size() - 1; k++) {
           int minIndex = k;
           for(int j = k + 1; j < elements.size(); j++) {
               if((direction.getDirection() * elements.get(j).compareTo(elements.get(minIndex))) < 0) {
                   minIndex = j;
               }
           }
           T temp = elements.get(k);
           elements.set(k, elements.get(minIndex));
           elements.set(minIndex, temp);
       }
        return elements;
    }
}
