package com.geekhub.hw4.task2;

public class User implements Comparable<User> {
    private final String name;
    private final int age;

    User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return " " + this.name + this.age;
    }

    @Override
    public int compareTo(User o) {
        int result;
        result = name.compareTo(o.name);
        if (result != 0) {
            return result;
        }
        return Integer.compare(age, o.age);
    }
}
