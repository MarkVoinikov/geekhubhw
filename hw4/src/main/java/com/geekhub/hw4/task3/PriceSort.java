package com.geekhub.hw4.task3;

import java.util.Comparator;

public class PriceSort implements Comparator<Product> {

    @Override
    public int compare(Product o1, Product o2) {
        if (o1.price == o2.price) {
            return 0;
        }
        if (o1.price > o2.price) {
            return 1;
        } else {
            return -1;
        }
    }
}


