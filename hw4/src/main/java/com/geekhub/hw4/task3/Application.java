package com.geekhub.hw4.task3;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Product Milk = new Product("Milk", 27, 200);
        Product Bread = new Product("Bread", 10, 500);
        Product Meat = new Product("Meat", 100, 300);

        Inventory magazine = new Inventory();
        magazine.addProduct(Milk);
        magazine.addProduct(Bread);
        magazine.addProduct(Meat);
        System.out.println("Write new product name, price, pcs");
        magazine.addProduct(new Product(input(), inputValue(), inputValue()));

        Milk.decPcs(10);
        Bread.setPrice(24);
        Meat.incPcs(500);
        Milk.setPrice(50);

        magazine.printInventory();
        magazine.printSortPrice();
        magazine.printSumInventory();
    }

    private static String input() {
        Scanner reader = new Scanner(System.in);
        return reader.nextLine();
    }

    private static int inputValue() {
        Scanner reader = new Scanner(System.in);
        String number = reader.nextLine();
        return Integer.parseInt(number);
    }
}
