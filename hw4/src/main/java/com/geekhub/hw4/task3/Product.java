package com.geekhub.hw4.task3;

public class Product {
    private String name;
    int price;
    int pcs;

    Product(String name, int price, int pcs) {
        this.name = name;
        this.price = price;
        this.pcs = pcs;
    }

    void incPcs(int pcs) {
        this.pcs += pcs;
    }

    void decPcs(int pcs) {
        this.pcs = Math.max(this.pcs - pcs, 0);
    }

    int getPcs() {
        return pcs;
    }

    void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuffer buf = new StringBuffer("Product{");
        buf.append("name=").append(name);
        buf.append(", price=").append(price);
        buf.append(", pcs='").append(pcs);
        buf.append('}');
        return buf.toString();
    }
}
