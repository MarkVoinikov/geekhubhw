package com.geekhub.hw4.task3;

import java.util.*;

class Inventory {
    private List<Product> products;

    Inventory() {
        products = new ArrayList<>();
    }

    void addProduct(Product p) {
        products.add(p);
    }

    void printInventory() {
        System.out.println("PrintInventory:");
        for (Product p : products) {
            System.out.println(p);
        }
    }

    void printSortPrice() {
        System.out.println("PrintSortPrice:");
        PriceSort sort = new PriceSort();
        products.sort(sort);
        for (Product s : products) {
            System.out.println(s);
        }
    }

    void printSumInventory() {
        double sum = 0;
        for(Product p : products) {
            sum += p.price * p.getPcs();
        }
        System.out.println("TotalSum: " + sum);
    }

    private static String input() {
        Scanner reader = new Scanner(System.in);
        return reader.nextLine();
    }

    private static int inputValue() {
        Scanner reader = new Scanner(System.in);
        String number = reader.nextLine();
        return Integer.parseInt(number);
    }
}

