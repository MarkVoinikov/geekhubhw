package com.geekhub.hw4.task3;

import java.util.Comparator;

public class PcsSort implements Comparator<Product> {

    @Override
    public int compare(Product o1, Product o2) {
        if (o1.pcs == o2.pcs) {
            return 0;
        }
        if (o1.pcs > o2.pcs) {
            return 1;
        } else {
            return -1;
        }
    }
}

