CREATE DATABASE geekhub;
CREATE TABLE customers
(
  Id INTEGER NOT NULL DEFAULT 1,
  FirstName VARCHAR(40),
  LastName VARCHAR(40),
  CellPhone VARCHAR(30),
  CONSTRAINT customers_pkey PRIMARY KEY (Id)
);
CREATE TABLE product
(
  Id SERIAL PRIMARY KEY,
  Name VARCHAR(40),
  Description VARCHAR(40),
  CurrentPrice NUMERIC
);
CREATE TABLE orders
(
  Customer INTEGER REFERENCES customers (Id),
  Products CHARACTER VARYING REFERENCES product (Name),
  Quantity INTEGER DEFAULT 1,
  Price NUMERIC,
  DeliveryPlace VARCHAR(100)
);
INSERT INTO Orders(Customer, Products, Quantity, Price, DeliveryPlace)
VALUES
       (
           (SELECT Id FROM customers WHERE CellPhone='+380931111110'),
           (SELECT Name FROM product WHERE Id='81'),
           2,
           (SELECT CurrentPrice FROM product WHERE Id='81'),
           'Cherkassy_Shevchenks_Str.1'
           ),
       (
           (SELECT Id FROM customers WHERE CellPhone='+380931111111'),
           (SELECT Name FROM product WHERE Id='81'),
           11,
           (SELECT CurrentPrice FROM product WHERE Id='81'),
           'Cherkassy_Shevchenks_Str.1'
           ),
       (
           (SELECT Id FROM customers WHERE CellPhone='+380931111110'),
           (SELECT Name FROM product WHERE Id='85'),
           3,
           (SELECT CurrentPrice FROM product WHERE Id='85'),
           'Cherkassy_Shevchenks_Str.1'
           );


SELECT LastName, SUM(Orders.Quantity * Orders.Price)
FROM Customers JOIN Orders ON Orders.Customer = Customers.Id
GROUP BY Customers.Id, Customers.LastName ;
