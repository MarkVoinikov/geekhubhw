package com.geekhub.hw10;

import java.sql.*;

public class Application {

    public static void main(String[] args) {
        try {
            String url = "jdbc:postgresql://localhost:5432/geekhub";
            String user = "postgres";
            String password = "postgres";

            try (Connection con = DriverManager.getConnection(url, user, password)) {
                TablesFilling.addCustomers(con);
                TablesFilling.addProduct(con);
            }
        } catch (SQLException e) {
            System.out.println("Error Connection");
        }
    }
 }



