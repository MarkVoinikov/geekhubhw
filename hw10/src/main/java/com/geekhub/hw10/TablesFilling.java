package com.geekhub.hw10;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TablesFilling {
    public static void addCustomers(Connection con) throws SQLException {
        try (PreparedStatement stmt = con.prepareStatement(
                "INSERT INTO customers (firstname, lastname, cellphone)"
                        + "VALUES (?, ?, ?)")) {
            for (int i = 0; i < 10; i++) {
                stmt.setString(1, "Steven" + i);
                stmt.setString(2, "Sigal" + i);
                stmt.setString(3, "+38093111111" + i);
                stmt.addBatch();
            }
            stmt.executeBatch();
        }
    }

    public static void addProduct(Connection conn) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement(
                "INSERT INTO product (name, description, currentprice) "
                        + "VALUES (?, ?, ?)")) {
            for (int i = 0; i < 10; i++) {
                stmt.setString(1, "StarPower" + i);
                stmt.setString(2, "It can increase strength on " + i + "powers");
                stmt.setInt(3,  10 + i);
                stmt.addBatch();
            }
            stmt.executeBatch();
        }
    }
}
