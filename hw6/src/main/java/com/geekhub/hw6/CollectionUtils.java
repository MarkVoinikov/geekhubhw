package com.geekhub.hw6;

import java.util.*;
import java.util.function.*;
import java.util.function.Predicate;

public class CollectionUtils {

    private CollectionUtils() {
    }

    public static <E> List<E> generate(Supplier<E> generator, Supplier<List<E>> listFactory, int count) {
        List<E> list = listFactory.get();
        for (int i = 0; i < count; ++i) {
            list.add(generator.get());
        }
        return list;
    }

    public static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> list = new ArrayList<>();
        for (E ele : elements) {
            if (filter.test(ele)) {
                list.add(ele);
            }
        }
        return list;
    }

    public static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E list : elements) {
            if (predicate.test(list)) {
                return true;
            }
        }
        return false;
    }

    public static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E list : elements) {
            if (!predicate.test(list)) {
                return false;
            }
        }
        return true;
    }

    public static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        return !anyMatch(elements, predicate);
    }

    public static <T, R> List<R> map(List<T> elements, Function<T, R> mappingFunction, Supplier<List<R>> listFactory) {
        List<R> list = listFactory.get();
        for (T element : elements) {
            list.add(mappingFunction.apply(element));
        }
        return list;
    }


    public static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        Iterator<E> iterator = elements.iterator();
        if (elements.isEmpty()) {
            return Optional.empty();
        }
        E maxElement = iterator.next();
        while (iterator.hasNext()) {
            if (comparator.compare(maxElement, iterator.next()) < 0) {
                maxElement = iterator.next();
            }
        }
        return Optional.of(maxElement);
    }

    public static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }
        E minElement = elements.iterator().next();
        for (E element : elements) {
            if (comparator.compare(minElement, element) > 0) {
                minElement = element;
            }
        }
        return Optional.ofNullable(minElement);
    }

    public static <E> List<E> distinct(List<E> elements, Supplier<List<E>> listFactory) {
        List<E> list = listFactory.get();
        for (E element : elements) {
            if (!list.contains(element)) {
                list.add(element);
            }
        }
        return list;
    }

    public static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E list : elements) {
            consumer.accept(list);
        }
    }

    public static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        if (elements.isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable(reduce(elements.get(0), elements, accumulator));
    }

    public static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E result = seed;
        for (E item : elements) {
            result = accumulator.apply(result, item);
        }
        return result;
    }

    public static <E> Map<Boolean, List<E>> partitionBy(List<E> elements, Predicate<E> predicate,
                                                        Supplier<Map<Boolean, List<E>>> mapFactory,
                                                        Supplier<List<E>> listFactory) {
        Map<Boolean, List<E>> map = mapFactory.get();
        List<E> resultTrue = listFactory.get();
        List<E> resultFalse = listFactory.get();
        for (E current : elements) {
            if (predicate.test(current)) {
                resultTrue.add(current);
            } else {
                resultFalse.add(current);
            }
            map.put(Boolean.TRUE, resultTrue);
            map.put(Boolean.FALSE, resultFalse);
        }
        return map;
    }

    public static <T, K> Map<K, List<T>> groupBy(List<T> elements,
                                                 Function<T, K> classifier,
                                                 Supplier<Map<K, List<T>>> mapFactory,
                                                 Supplier<List<T>> listFactory) {
        List<T> list = listFactory.get();
        Map<K, List<T>> map = mapFactory.get();
        for (T element : elements) {
            K key = classifier.apply(element);
            if (map.containsKey(key)) {
                map.get(key).add(element);
            } else {
                list.add(element);
                map.put(key, list);
            }
        }
        return map;
    }

    public static <T, K, U> Map<K, U> toMap(List<T> elements,
                                            Function<T, K> keyFunction,
                                            Function<T, U> valueFunction,
                                            BinaryOperator<U> mergeFunction,
                                            Supplier<Map<K, U>> mapFactory) {
        Map<K, U> map = mapFactory.get();
        for (T element : elements) {
            K key = keyFunction.apply(element);
            U value = valueFunction.apply(element);
            if (map.containsKey(key)) {
                value = mergeFunction.apply(map.get(key), value);
                map.put(key, value);
            } else {
                map.put(key, value);
            }
        }
        return map;
    }


    //ADDITIONAL METHODS

    public static <E, T> Map<Boolean, List<T>> partitionByAndMapElement(List<E> elements,
                                                                        Predicate<E> predicate,
                                                                        Supplier<Map<Boolean, List<T>>> mapFactory,
                                                                        Supplier<List<T>> listFactory,
                                                                        Function<E, T> elementMapper) {
        List<T> resultTrue = listFactory.get();
        List<T> resultFalse = listFactory.get();
        Map<Boolean, List<T>> map = mapFactory.get();
        for (E element : elements) {
            if (predicate.test(element)) {
                resultTrue.add(elementMapper.apply(element));
            } else {
                resultFalse.add(elementMapper.apply(element));
            }
            map.put(Boolean.TRUE, resultTrue);
            map.put(Boolean.FALSE, resultFalse);
        }
        return map;
    }

    public static <T, U, K> Map<K, List<U>> groupByAndMapElement(List<T> elements,
                                                                 Function<T, K> classifier,
                                                                 Supplier<Map<K, List<U>>> mapFactory,
                                                                 Supplier<List<U>> listFactory,
                                                                 Function<T, U> elementMapper) {
        List<U> list = listFactory.get();
        Map<K, List<U>> map = mapFactory.get();
        for (T element : elements) {
            K key = classifier.apply(element);
            if (map.containsKey(key)) {
                map.get(key).add(elementMapper.apply(element));
            } else {
                list.add(elementMapper.apply(element));
                map.put(key, list);
            }
        }
        return map;
    }
}

