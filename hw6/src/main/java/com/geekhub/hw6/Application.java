package com.geekhub.hw6;

import com.sun.applet2.AppletParameters;

import java.util.*;
import java.util.ArrayList;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Hell");
        list.add("Burn");
        list.add("small");
        list.add("Pi");
        list.add("Not_Name");
        //list.add("It_be_must_can_maybe_see");
        list.add("I don't know what I know");
        list.add("small");
        list.add("l");

        //List<String> list2 = CollectionUtils.generate(() -> "it is very hard HW", ArrayList::new, 10);
        //System.out.println(CollectionUtils.generate(() -> "hello", ArrayList::new, 10));
        //System.out.println(CollectionUtils.filter(list2, s -> s.startsWith("i")));
        //System.out.println(CollectionUtils.anyMatch(list, s -> s.contentEquals("hw1")));
        //System.out.println(CollectionUtils.allMatch(list2, s -> s.contentEquals("it is very hard HW")));
        //System.out.println(CollectionUtils.noneMatch(list, s -> s.contentEquals("hw1")));
        //System.out.println(CollectionUtils.map(list, s -> s + " haha", ArrayList::new));
        System.out.println(CollectionUtils.max(list, Comparator.comparingInt(String::length)));
        System.out.println(CollectionUtils.min(list, Comparator.comparingInt(String::length)));
        //System.out.println(CollectionUtils.distinct(list, ArrayList::new));
        //CollectionUtils.forEach(list, System.out::println);
        //System.out.println(CollectionUtils.reduce(list, (s, s2) -> s + ", " + s2));
        /*System.out.println(CollectionUtils.reduce("", list, ((s, s2) -> s + s2)));
        System.out.println(CollectionUtils.partitionBy(list, s -> s.length() <= 5, AppletParameters::new,
              ArrayList::new));
        System.out.println(CollectionUtils.groupBy(list, String::length, AppletParameters::new, ArrayList::new));
        System.out.println(CollectionUtils.toMap(list, String::length, value -> value.substring(1),
                (s, s2) -> s + " * " + s, AppletParameters::new));
        System.out.println(CollectionUtils.partitionByAndMapElement(list, s -> s.startsWith("h"),
                AppletParameters::new, ArrayList::new, s -> s + " " + s.hashCode()));
        System.out.println(CollectionUtils.groupByAndMapElement(list, String::length, AppletParameters::new,
                ArrayList::new, String::hashCode));
    */
    }

}
