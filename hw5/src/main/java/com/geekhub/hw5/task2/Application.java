package com.geekhub.hw5.task2;

import java.util.*;

public class Application {

    public static void main(String[] args) {
        System.out.println("Write a few words");
        String a = input();
        List<String> list = new LinkedList<>(Arrays.asList(a.split("[,;:.!?\\s]+")));
        List<String> sortList = new LinkedList<>();

        for (String s : list) {
            if (s.length() > 9) {
                List<String> list1 = new LinkedList<>(Arrays.asList(s.split("(?<=\\G.{1})")));
                s = list1.get(0) + (list1.size() - 2) + list1.get(list1.size() - 1);
            }
            sortList.add(s);
        }
        System.out.println(sortList);
    }

    private static String input() {
        Scanner reader = new Scanner(System.in);
        return reader.nextLine();
    }
}
