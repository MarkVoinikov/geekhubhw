package com.geekhub.hw5.task1;

import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Tasks {
    private Date date;
    private PriorityTask priority;
    private String description;
    private int compFlag;

    private static DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

    Tasks (String d, PriorityTask priority, String description, int compFlag) throws ParseException {
        date = df.parse(d);
        this.priority = priority;
        this.description = description;
        this.compFlag = compFlag;
    }

    void setDate(String s) {
        try {
            date = df.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    String getDate() {
        return df.format(date);
    }

    PriorityTask getPriority() {
        return priority;
    }

    int getCompFlag() {
        return compFlag;
    }

    void setCompFlag(int compFlag) {
        this.compFlag = compFlag;
    }

    @Override
    public String toString() {
        return "Task{" + "date=" + date +
                ", priority=" + priority +
                ", description=" + description +
                ", completed flag=" + compFlag +
                '}';
    }
}
