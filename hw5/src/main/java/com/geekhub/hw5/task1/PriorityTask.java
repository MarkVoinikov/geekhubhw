package com.geekhub.hw5.task1;

public enum PriorityTask {
    HIGH(1), MEDIUM(2), LOW(3);

    private int value;

    PriorityTask (int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}

