package com.geekhub.hw5.task1;

import java.text.ParseException;

public class Application {

    public static void main(String[] args) throws ParseException {
        Tasks task1 = new Tasks("12/10/2018", PriorityTask.MEDIUM, "Go Home", 0);
        Tasks task2 = new Tasks("10/10/2018", PriorityTask.LOW, "Go Work", 0);
        Tasks task3 = new Tasks("12/09/2018", PriorityTask.LOW, "Go Dance", 0);
        Tasks task4 = new Tasks("12/09/2018", PriorityTask.MEDIUM, "Go Dance home", 0);
        Tasks task5 = new Tasks("11/09/2018", PriorityTask.HIGH, "Go Work maybe", 1);

        Scheduler manager = new Scheduler();
        manager.addTask(task1);
        manager.addTask(task2);
        manager.addTask(task3);
        manager.addTask(task4);
        manager.addTask(task5);

        task1.setCompFlag(1);
        task2.setDate("10/11/2018");
        task3.setCompFlag(0);
        manager.printTasks();
        manager.dateSort();
        manager.deleteTasks();
        manager.prioritySort();
        manager.dateCompTasksSort();

    }
}
