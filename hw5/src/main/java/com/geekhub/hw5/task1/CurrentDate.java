package com.geekhub.hw5.task1;

import java.text.SimpleDateFormat;

class CurrentDate {
    private long curTime = System.currentTimeMillis();
    private String curStringDate = new SimpleDateFormat("MM.dd.yyyy").format(curTime);

    String getCurStringDate() {
        return curStringDate;
    }
}
