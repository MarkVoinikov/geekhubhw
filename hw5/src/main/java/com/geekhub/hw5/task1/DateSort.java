package com.geekhub.hw5.task1;

import java.util.Comparator;

public class DateSort implements Comparator<Tasks> {

    @Override
    public int compare(Tasks o1, Tasks o2) {
        if (o1.getDate() == null || o2.getDate() == null) {
            return 0;
        } else if (o1.getDate().equals(o2.getDate())) {
            PrioritySort prioritySort = new PrioritySort();
            return prioritySort.compare(o1, o2);
        }
        return o1.getDate().compareTo(o2.getDate());
    }
}
