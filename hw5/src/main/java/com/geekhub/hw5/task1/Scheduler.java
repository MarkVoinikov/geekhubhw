package com.geekhub.hw5.task1;

import java.util.ArrayList;
import java.util.List;

class Scheduler {
    private List<Tasks> tasks = new ArrayList<>();
    private List<Tasks> completeTasks = new ArrayList<>();

    void addTask(Tasks t) {
        tasks.add(t);
    }

    void deleteTasks() {
        CurrentDate curDate = new CurrentDate();
        System.out.println("Delete all complete and overdue tasks:");
        for (Tasks t : tasks) {
            if (t.getCompFlag() > 0 || (t.getDate().compareTo(curDate.getCurStringDate()) <= 0)) {
                completeTasks.add(t);
                System.out.println(t);
            }
        }
        tasks.removeIf(nextTasks -> (nextTasks.getCompFlag() > 0) || nextTasks.getDate().compareTo
                (curDate.getCurStringDate()) <= 0);
    }

    void printTasks() {
        System.out.println("PrintTasks:");
        for (Tasks t : tasks) {
            System.out.println(t);
        }
    }

    void prioritySort() {
        System.out.println("Sort by priority:");
        PrioritySort priority = new PrioritySort();
        tasks.sort(priority);
        for (Tasks t : tasks) {
            System.out.println(t);
        }
    }

    void priorCompTasksSort() {
        System.out.println("Sort complete tasks by priority:");
        PrioritySort priority = new PrioritySort();
        completeTasks.sort(priority);
        for (Tasks t : completeTasks) {
            System.out.println(t);
        }
    }

    void dateSort() {
        System.out.println("Sort by date:");
        DateSort dateSort = new DateSort();
        tasks.sort(dateSort);
        for (Tasks t : tasks) {
            System.out.println(t);
        }
    }

    void dateCompTasksSort() {
        System.out.println("Sort complete tasks by date:");
        DateSort dateSort = new DateSort();
        completeTasks.sort(dateSort);
        for (Tasks t : completeTasks) {
            System.out.println(t);
        }
    }
}
