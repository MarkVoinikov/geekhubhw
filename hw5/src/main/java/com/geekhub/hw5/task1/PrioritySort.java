package com.geekhub.hw5.task1;

import java.util.Comparator;

public class PrioritySort implements Comparator<Tasks> {

    @Override
    public int compare(Tasks o1, Tasks o2) {
        if (o1.getPriority() == o2.getPriority()) {
            return 0;
        }
        return (o1.getPriority()).compareTo(o2.getPriority());
    }
}
