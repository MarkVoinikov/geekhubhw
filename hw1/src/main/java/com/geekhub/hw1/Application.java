package com.geekhub.hw1;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        String NUMBER_VALIDITY = "^((\\+38|8|38)?(067|096|097|098|050|066|095|099|063|073|093|091|092|094|)\\d{7})$";
        String number;
        int sumNumber1 = 0;
        int sumNumber2 = 0;
        int sumNumber3 = 0;
        boolean isValid;

        System.out.println("Please enter the phone number:");
        Scanner reader = new Scanner(System.in);
        number = reader.nextLine();
        isValid = number.matches(NUMBER_VALIDITY);
        while (!isValid) {
            System.out.println("Phone number is incorrect.Please enter the phone number:");
            number = reader.nextLine();
            isValid = number.matches(NUMBER_VALIDITY);
        }
        System.out.println("Phone number is correct.");
        number = number.replace("+", "0");
        String[] a = number.split("");
        for (int i = 1; i < a.length; i++) {
            sumNumber1 += Integer.parseInt(a[i]);
        }
        sumNumber2 = calcSumNumbers(sumNumber1, sumNumber2);
        sumNumber3= calcSumNumbers(sumNumber2, sumNumber3);
        System.out.println("1st round of calculation, sum is:" + sumNumber1);
        System.out.println("2st round of calculation, sum is:" + sumNumber2);
        if (sumNumber2 % 10 == 0) {
            System.out.println("3st round of calculation, sum is:" + sumNumber3);
        }
    }

    private static int calcSumNumbers(int a, int b) {
        while (a > 0) {
            b += a % 10;
            a /= 10;
        }
        return b;
    }
}
