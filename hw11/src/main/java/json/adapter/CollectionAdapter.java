package json.adapter;

import json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {

    @Override
    public Object toJson(Collection c) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (Object i : c) {
            jsonArray.put(JsonSerializer.serialize(i));
        }
        return jsonArray;
    }
}
