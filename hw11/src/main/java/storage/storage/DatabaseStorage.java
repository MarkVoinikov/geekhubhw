package storage.storage;

import storage.objects.Entity;
import storage.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link Entity} class.
 * Could be created only with {@link Connection} specified.
 */
public class DatabaseStorage implements Storage {

    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws StorageException {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM \"" + clazz.getSimpleName().toLowerCase() + "\" WHERE id =  " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws StorageException {
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase();
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql));
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws StorageException {
        if (entity.isNew()) {
            return false;
        }
        String sql = "DELETE FROM " + entity.getClass().getSimpleName().toLowerCase() + " WHERE id = "
                + entity.getId();
        try (Statement statement = connection.createStatement()) {
            return statement.executeUpdate(sql) > 0;
        } catch (Exception e) {
            throw new StorageException(e);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) {
        String sql;
        Map<String, Object> map = prepareEntity(entity);
        String entityName = entity.getClass().getSimpleName().toLowerCase();
        if (entity.isNew()) {
            String query = "INSERT INTO " + entityName + " (";
            sql = anyInsert(query, map);
            System.out.println(sql);
        } else {
            String query = "UPDATE " + entityName + " SET ";
            sql = anyUpdate(query, map) + entity.getId();
            System.out.println(sql);
        }

        try (PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            int i = 1;
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                pstmt.setObject(i++, entry.getValue());
            }
            pstmt.executeUpdate();
            if (entity.isNew()) {
                ResultSet rsKey = pstmt.getGeneratedKeys();
                if (rsKey.next()) {
                    entity.setId(rsKey.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) {
        Map<String, Object> prepareEntity = new HashMap<>();
        for(Field field : entity.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(Ignore.class)) {
                try {
                    prepareEntity.put(field.getName(), field.get(entity));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return prepareEntity;
    }

    private static String anyInsert(String sql, Map<String, Object> prepareEntity) {
        StringBuilder sb = new StringBuilder();
        for (String key : prepareEntity.keySet()) {
            sb.append(key);
            sb.append(", ");
        }
        sb.deleteCharAt(sb.length() - 2);
        sb.append(") VALUES(");
        sb.append(new String(new char[prepareEntity.size()]).replace("\0", "?,"));
        sb.setLength(Math.max(sb.length() - 1, 0));
        if (sb.length() > 1) {
            sql = sql.replace("(?)", "(" + sb + ")") + sb + ")";
        }
        return sql;
    }

    private static String anyUpdate(String sql, Map<String, Object> prepareEntity) {
        StringBuilder sb = new StringBuilder();
        for (String key : prepareEntity.keySet()) {
            sb.append(key);
            sb.append("= ?, ");
        }
        sb.deleteCharAt(sb.length() - 2);
        sb.append(" WHERE id=");
        return sql + sb;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> list = new ArrayList<>();
        while (resultSet.next()) {
            T object = clazz.newInstance();
            object.setId(resultSet.getInt("id"));
            for (Field field : clazz.getDeclaredFields()) {
                field.setAccessible(true);
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.set(object, resultSet.getObject(field.getName()));
                }
            }
            list.add(object);
        }
        return list;
    }
}
