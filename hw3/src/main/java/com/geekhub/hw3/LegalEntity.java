package com.geekhub.hw3;

class LegalEntity extends PrivateCostumer {
    private String edrpou;
    private String mfo;

    public void setEdrpou(String a) {
        edrpou = a;
    }

    public void setMfo(String a) {
        mfo = a;
    }

    @Override
    public String toString() {
        return super.toString() + ",\n EDRPO:" + edrpou + ", MFO: " + mfo;
    }
}
