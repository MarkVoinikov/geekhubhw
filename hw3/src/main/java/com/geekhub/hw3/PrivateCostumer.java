package com.geekhub.hw3;

class PrivateCostumer implements Client {
    private int indNumber;
    private String name;
    private String taxNumber;
    private String address;
    private int sum = 0;
    private int years = 0;


    void setIndNumber(int a) {
        indNumber = a;
    }

    public int getIndNumber() {
        return indNumber;
    }

    void setName(String a) {
        name = a;
    }

    public String getName() {
        return name;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getSum() {
        return sum;
    }

    void setYears(int years) {
        this.years = years;
    }

    void setTaxNumber(String a) {
        taxNumber = a;
    }

    void setAddress(String a) {
        address = a;
    }

    @Override
    public int sumDeposit() {
        return sum * (sum * 10 / 100 * years);
    }

    @Override
    public int convertGold(int pcs) {
        int GRAM_GOLD_TO_DOLLAR = 38;
        return GRAM_GOLD_TO_DOLLAR * pcs;
    }
    @Override
    public int convertEuro(int pcs) {
        int EURO_TO_DOLLAR = 2;
        return EURO_TO_DOLLAR * pcs;
    }

    public String toString() {
        return "Individual Number: " + getIndNumber() + ", Name: "+ getName() + ", Tax Number: " + taxNumber
                + ",\n Address: " + address + ", Active Sum "+ getSum() + ", Deposit term: " + years
                + ", After the end terms sum deposit is: " + sumDeposit();
    }
}
