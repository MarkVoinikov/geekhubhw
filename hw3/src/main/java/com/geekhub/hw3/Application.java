package com.geekhub.hw3;

import java.util.*;

public class Application {

    private static List<Client> list = new ArrayList<>();

    public static void main(String[] args) {
        addSecondClient();
        addFirstClient();
        addFirstClient();
        addSecondClient();
        boolean notExit = true;
        while (notExit) {
            System.out.println();
            System.out.println("Enter number you need: 1 - Add new client; 2 - View param client; 3 - Add new active; "
                    + "4 - View client active; 0 - Exit from the program");
            Menu arg = Menu.fromString(input());
            switch (arg) {
                case ADD_NEW_CLIENT:
                    totalActiveBank();
                    System.out.println("Write 1 - if you privet, 2 - if you legal");
                    int i = inputValue();
                    if (i == 1) {
                        addPrivateClient();
                    } else if (i == 2) {
                        addLegalClient();
                    } else {
                        System.out.println("Wrong input");
                    }
                    totalActiveBank();
                    break;
                case VIEW_CLIENT:
                    getIndNumber();
                    System.out.println(list.get(inputValue()));
                    totalActiveBank();
                    break;
                case ADD_NEW_ACTIVE:
                    totalActiveBank();
                    increaseBalance();
                    totalActiveBank();
                    break;
                case VIEW_CLIENT_ACTIVE:
                    getIndNumber();
                    int a = inputValue();
                    System.out.println("Client balance: " + list.get(a).getSum());
                    totalActiveBank();
                    break;
                case EXIT:
                    notExit = false;
                    break;
            }
        }
    }

    private static Client a;
    private static void increaseBalance() {
        getIndNumber();
        a = list.get(inputValue());
        System.out.println("Enter how much to increase the balance " + a.getName());
        int i = inputValue();
        a.setSum(a.getSum() + i + exchangeCurrency());
        System.out.println("Balance " + a.getName() + " is: " + a.getSum());
    }

    private static int exchangeCurrency() {
        System.out.println("Enter other currencies from the proposed: gold or euro");
        String x = input();
        System.out.println("Enter pcs");
        int y = inputValue();
        switch (x) {
            case "gold":
                return a.convertGold(y);
            case "euro":
                return a.convertEuro(y);
            default:
                return 0;
        }
    }

    private static void getIndNumber() {
        System.out.println("Enter client individual number: ");
        for (Client list1 : list) {
            System.out.print(list1.getIndNumber() + ", ");
        }
        System.out.println();
    }

    private static void totalActiveBank() {
        int i = 0;
        for (Client list1 : list) {
            i += list1.getSum();
        }
        System.out.println("All bank active is: " + i);
    }

    private static void addPrivateClient() {
        PrivateCostumer client = new PrivateCostumer();
        list.add(client);
        client.setIndNumber(list.indexOf(client));
        System.out.println("Enter name");
        client.setName(input());
        System.out.println("Enter tax number");
        client.setTaxNumber(input());
        System.out.println("Enter address");
        client.setAddress(input());
        System.out.println("Enter sum deposit");
        client.setSum(inputValue());
        System.out.println("Enter the time in the deposit years");
        client.setYears(inputValue());
        client.sumDeposit();
        System.out.println(list.get(list.indexOf(client)));

    }

    private static void addLegalClient() {
        LegalEntity client = new LegalEntity();
        list.add(client);
        client.setIndNumber(list.indexOf(client));
        System.out.println("Enter name");
        client.setName(input());
        System.out.println("Enter tax number");
        client.setTaxNumber(input());
        System.out.println("Enter EDRPOU");
        client.setEdrpou(input());
        System.out.println("Enter MFO");
        client.setMfo(input());
        System.out.println("Enter address");
        client.setAddress(input());
        System.out.println("Enter sum deposit");
        client.setSum(inputValue());
        System.out.println("Enter the time in the deposit years");
        client.setYears(inputValue());
        client.sumDeposit();
        System.out.println(list.get(list.indexOf(client)));
    }

    private static String input() {
        Scanner reader = new Scanner(System.in);
        return reader.nextLine();
    }

    private static int inputValue() {
        Scanner reader = new Scanner(System.in);
        String number = reader.nextLine();
        return Integer.parseInt(number);
    }

    private static void addFirstClient() {
        LegalEntity clients2 = new LegalEntity();
        list.add(clients2);
        clients2.setIndNumber(list.lastIndexOf(clients2));
        clients2.setName("PP Piroshok");
        clients2.setAddress("Ukrain, Cherkassy, Pirogova str. 11");
        clients2.setTaxNumber("545465454");
        clients2.setEdrpou("458562");
        clients2.setMfo("524163");
        clients2.setSum(23000);
        clients2.setYears(10);
        clients2.sumDeposit();
    }

    private static void addSecondClient() {
        PrivateCostumer clients1 = new PrivateCostumer();
        list.add(clients1);
        clients1.setIndNumber(list.indexOf(clients1));
        clients1.setName("Ivanov Ivan");
        clients1.setAddress("Ukrain, Cherkassy, Ivanova str. 12");
        clients1.setTaxNumber("12345678");
        clients1.setSum(2000);
        clients1.setYears(1);
        clients1.sumDeposit();
    }
}
