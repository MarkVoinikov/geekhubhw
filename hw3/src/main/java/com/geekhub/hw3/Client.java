package com.geekhub.hw3;

interface Client {

    int sumDeposit();

    int getSum();

    int getIndNumber();

    void setSum(int sum);

    String getName();

    int convertGold(int pcs);

    int convertEuro(int pcs);
}
