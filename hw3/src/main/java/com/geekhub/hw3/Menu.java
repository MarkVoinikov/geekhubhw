package com.geekhub.hw3;

public enum Menu {
    ADD_NEW_CLIENT("1"),
    VIEW_CLIENT("2"),
    ADD_NEW_ACTIVE("3"),
    VIEW_CLIENT_ACTIVE("4"),
    EXIT("0");

    public String parameterName;

    Menu(String parameterName) {
        this.parameterName = parameterName;
    }

    public static Menu fromString(String parameterName) throws NullPointerException {
        if (parameterName != null) {
            for (Menu objType : Menu.values()) {
                if (parameterName.equalsIgnoreCase(objType.parameterName)) {
                    return objType;
                }
            }
        }
        throw new NullPointerException("Wrong input");
    }
}
