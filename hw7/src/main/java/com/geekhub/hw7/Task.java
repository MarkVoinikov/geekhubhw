package com.geekhub.hw7;

import com.geekhub.hw7.TaskType;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

public class Task {
    private int id ;
    private TaskType type ;
    private String title ;
    private boolean done ;
    private Set<String> categories;
    private LocalDate startsOn ;

    Task (int id, TaskType type, String title, boolean done, Set<String> categories, LocalDate startsOn) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.done = done;
        this.categories= categories;
        this.startsOn = startsOn;
    }

    public void setCategories(Set<String> categories) {
        this.categories = categories;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public TaskType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public Set<String> getCategories() {
        return categories;
    }

    public LocalDate getStartsOn() {
        return startsOn;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
    public String toString() {
        return "Task{" + "id=" + id +
                ", type=" + type +
                ", title=" + title +
                ", done=" + done +
                ", categories=" + categories +
                ", startsOn=" + startsOn +
                '}';
    }
}
