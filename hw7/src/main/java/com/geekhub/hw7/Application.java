package com.geekhub.hw7;

import java.time.LocalDate;
import java.util.*;

public class Application {
    private static List<Task> createScheduler() {
        return Arrays.asList(
                new Task(1, TaskType.HIGH, "blablabla", false, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(1)),
                new Task(2, TaskType.HIGH, "blablabla12334", false, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(4)),
                new Task(3, TaskType.LOW, "blablablafdhdfb", true, new HashSet<>(Collections.singletonList("fridayregtrhtrhytjyt")),
                        LocalDate.now().plusDays(2)),
                new Task(4, TaskType.HIGH, "blablabladvbbdbfg", false, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(4)),
                new Task(5, TaskType.REGULAR, "blablabladcssdcd", true, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(5)),
                new Task(6, TaskType.HIGH, "blablablafdbdfbgf", false, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(1)),
                new Task(7, TaskType.LOW, "blablabladfbfdbfd", false, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(2)),
                new Task(8, TaskType.HIGH, "blablabladfbdfbdf", true, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(3)),
                new Task(9, TaskType.HIGH, "blablabladfbdfbdf", true, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(3)),
                new Task(10, TaskType.REGULAR, "blablabladfbdfbdf", false, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(1)),
                new Task(11, TaskType.HIGH, "blablabladfbdfbdf", false, new HashSet<>(Collections.singletonList("monday")),
                        LocalDate.now().plusDays(1))
        );
    }

    public static void main(String[] args) {
        List<Task> tasks = Application.createScheduler();

        System.out.println(TaskManager.getUniqueCategories(tasks));
        System.out.println(TaskManager.find5NearestImportantTasks(tasks));
        System.out.println(TaskManager.getCategoriesWithTasks(tasks));
        System.out.println(TaskManager.splitTasksIntoDoneAndInProgress(tasks));
        System.out.println(TaskManager.exitsTaskOfCategory(tasks, "monday"));
        System.out.println(TaskManager.getTitlesOfTasks(tasks, 2, 4));
        System.out.println(TaskManager.getCountsByCategories(tasks));
        System.out.println(TaskManager.getCategoriesNamesLengthStatistics(tasks));
        System.out.println(TaskManager.findTaskWithBiggestCountOfCategories(tasks));
    }
}
