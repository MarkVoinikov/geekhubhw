package com.geekhub.hw7;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

class TaskManager {
    public static List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
                .flatMap(i -> i.getCategories().stream())
                .distinct()
                .collect(toList());
    }

    public static List<Task> find5NearestImportantTasks(List<Task> tasks) {
        return tasks.stream()
                .filter(task -> !task.isDone() && task.getType().equals(TaskType.HIGH))
                .sorted(Comparator.comparing(Task::getStartsOn, Comparator.nullsFirst(Comparator.naturalOrder())))
                .limit(5)
                .collect(toList());
    }

    public static Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return tasks.stream().collect(Collectors.groupingBy(i -> i.getCategories().toString()));
    }

    public static Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream().collect(Collectors.groupingBy(Task::isDone));
    }

    public static boolean exitsTaskOfCategory(List<Task> tasks, String category) {
        return tasks.stream()
                .filter(i -> !i.getCategories().isEmpty())
                .flatMap(i -> i.getCategories().stream())
                .allMatch(i -> i.equals(category));
    }

    public static String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream()
                .filter(i -> i.getId() >= startNo && i.getId() <= endNo)
                .map(Task::getTitle)
                .collect(Collectors.joining(", "));
    }


    public static Map<String, Long> getCountsByCategories(List<Task> tasks) {
        return tasks.stream()
                .filter(i -> !i.getCategories().isEmpty())
                .collect(Collectors.groupingBy(i -> i.getCategories().toString(), Collectors.counting()));
    }

    public static IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return tasks.stream()
                .filter(i -> !i.getCategories().isEmpty())
                .collect(Collectors.summarizingInt(s -> s.getCategories().toString().length()));
    }

    public static Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
        return tasks.stream()
                .filter(i -> !i.getCategories().isEmpty())
                .max(Comparator.comparing(i -> i.getCategories().toString().length(), Comparator.naturalOrder())).get();
    }
}
