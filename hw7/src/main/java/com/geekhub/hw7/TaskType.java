package com.geekhub.hw7;

public enum TaskType {
    HIGH,
    REGULAR,
    LOW;

    private int value;

    public int getValue() {
        return value;
    }
}
